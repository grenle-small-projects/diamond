(use-modules (srfi srfi-64)
             (proba commands)
             (codec))

(test-group
 "non existent codec"
 (test-equal #f (codec-from-char #\3))
 (test-equal #f (codec-from-char #\𒊿)))

(test-group
 "latin lowercase"
 (test-equal #\a ((codec-from-char #\a) 'index->char 0))
 (test-equal 0   ((codec-from-char #\a) 'char->index #\a))
 (test-equal #\k ((codec-from-char #\a) 'index->char 10))
 (test-equal 10  ((codec-from-char #\a) 'char->index #\k)))

(test-group
 "latin uppercase"
 (test-equal #\A ((codec-from-char #\A) 'index->char 0))
 (test-equal 0   ((codec-from-char #\A) 'char->index #\A))
 (test-equal #\K ((codec-from-char #\A) 'index->char 10))
 (test-equal 10  ((codec-from-char #\A) 'char->index #\K)))

(test-group
 "greek lowercase"
 (test-equal #\α ((codec-from-char #\α) 'index->char 0))
 (test-equal 0   ((codec-from-char #\α) 'char->index #\α))
 (test-equal #\η ((codec-from-char #\α) 'index->char 6))
 (test-equal 6   ((codec-from-char #\α) 'char->index #\η)))

(test-group
 "greek uppercase"
 (test-equal #\Α ((codec-from-char #\Α) 'index->char 0))
 (test-equal 0   ((codec-from-char #\Α) 'char->index #\Α))
 (test-equal #\Η ((codec-from-char #\Α) 'index->char 6))
 (test-equal 6   ((codec-from-char #\Α) 'char->index #\Η)))
