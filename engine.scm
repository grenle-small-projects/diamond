(define-module (engine)
  #:export (full-diamond diamond))

(use-modules ((utils)
              #:prefix utils:)
             (codec))

(define (make-space k) (make-list k #\space))
(define (cdr-reverse xs) (cdr (reverse xs)))

(define (top-left-quarter codec char)
  (let* ((height (codec 'char->index char))
         (right-space (utils:naturals-to height))
         (left-space (reverse right-space)))
    (map utils:flatten
     (utils:zip
      (map make-space left-space)
      (map (lambda (k) (list (codec 'index->char k))) right-space)
      (map make-space right-space)))))

(define (add-top-right top-left)
  (map utils:flatten
   (utils:zip
    top-left
    (map cdr-reverse top-left))))

(define (add-bottom top) (append top (cdr-reverse top)))

(define (full-diamond char)
  (let ((codec (codec-from-char char)))
    (if codec
        (map
         list->string
         (add-bottom
          (add-top-right
           (top-left-quarter (codec-from-char char) char))))
        #f)))

(define (diamond char)
  (let ((lines (full-diamond char)))
    (if lines
        (string-join
         (full-diamond char) "\n" 'suffix)
        #f)))

