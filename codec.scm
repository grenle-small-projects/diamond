(define-module (codec)
  #:export (make-codec
            codec-from-char))

;; assuming the ordering of the code point and the letter are the same
(define (make-codec first-letter last-letter)
  (lambda (operation-name operation-arg)
    (cond ((eq? operation-name 'char->index)
           (-
            (char->integer operation-arg)
            (char->integer first-letter)))
          ((eq? operation-name 'index->char)
           (integer->char
            (+ operation-arg (char->integer first-letter))))
          ((eq? operation-name 'in-range?)
           (and (char>=? operation-arg first-letter)
                (char<=? operation-arg last-letter)))
          (else (error "Don't know operation: ")))))

(define codecs
  (list (make-codec #\a #\z)   ; latin lowercase
        (make-codec #\A #\Z)   ; latin uppercase
        (make-codec #\α #\ω)   ; greek lowercase
        (make-codec #\Α #\Ω))) ; greek uppercase

(define (*codec-from-char char codecs)
  (if (null? codecs) #f
      (let ((codec (car codecs)))
        (if (codec 'in-range? char)
            codec
            (*codec-from-char char (cdr codecs))))))

(define (codec-from-char char) (*codec-from-char char codecs))

