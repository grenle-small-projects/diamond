(use-modules (srfi srfi-64)
             (proba commands)
             (utils))

(test-group
 "any?"
 (test-assert (any? list? '(() b c)))
 (test-assert (any? list? '(a () c)))
 (test-assert (any? list? '(a b ())))
 (test-assert (not (any? list? '())))
 (test-assert (not (any? list? '(a b c)))))

(test-group
 "any-null?"
 (test-assert (not (any-null? '())))
 (test-assert (any-null? '(())))
 (test-assert (not (any-null? '((a)))))
 (test-assert (any-null? '(() (a))))
 (test-assert (any-null? '((a) ()))))


(test-group
 "zip"
 (test-equal '() (zip '()))
 (test-equal '() (zip '(a) '()))
 (test-equal '() (zip '() '(a)))
 (test-equal '((a)) (zip '(a)))
 (test-equal '((a x)) (zip '(a) '(x)))
 (test-equal '((a x)) (zip '(a b) '(x)))
 (test-equal '((a x)) (zip '(a) '(x y)))
 (test-equal '((a x) (b y)) (zip '(a b) '(x y)))
 (test-equal '((a x 1) (b y 2)) (zip '(a b c) '(x y) '(1 2 3))))


(test-group
 "make-list"
 (test-equal '() (make-list 0 'a))
 (test-equal '(a) (make-list 1 'a))
 (test-equal '(1 1) (make-list 2 1))
 (test-equal '("z" "z" "z") (make-list 3 "z")))


(test-group
 "range"
 (test-equal '(0) (range 0 0))
 (test-equal '(2) (range 2 2))
 (test-equal '(0 1) (range 0 1))
 (test-equal '(3 4) (range 3 4))
 (test-equal '(0 -1 -2) (range 0 -2))
 (test-equal '(-2 -1 0) (range -2 0))
 (test-equal '(3 2 1) (range 3 1)))

(test-group
 "naturals-to"
 (test-equal '(0) (naturals-to 0))
 (test-equal '(0 1 2 3) (naturals-to 3)))

(test-group
 "interleave"
 (test-equal '() (interleave '() '()))
 (test-equal '() (interleave '(a) '()))
 (test-equal '() (interleave '() '(x)))
 (test-equal '(a x) (interleave '(a) '(x)))
 (test-equal '(a x) (interleave '(a b) '(x)))
 (test-equal '(a x) (interleave '(a) '(x y)))
 (test-equal '(a x b y) (interleave '(a b) '(x y))))

(test-group
 "interleave-greedy"
 (test-equal '()        (interleave-greedy '() '()))
 (test-equal '(a)       (interleave-greedy '(a) '()))
 (test-equal '(x)       (interleave-greedy '() '(x)))
 (test-equal '(a x)     (interleave-greedy '(a) '(x)))
 (test-equal '(a x b)   (interleave-greedy '(a b) '(x)))
 (test-equal '(a x y)   (interleave-greedy '(a) '(x y)))
 (test-equal '(a x b y) (interleave-greedy '(a b) '(x y))))

(test-group
 "flatten"
 (test-equal '() (flatten '()))
 (test-equal '(a) (flatten '(a)))
 (test-equal '(a) (flatten '((a))))
 (test-equal '(a (b) c) (flatten '((a) ((b)) c))))

