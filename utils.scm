(define-module (utils)
  #:export (any?
            any-null?
            zip
            range
            naturals-to
            interleave
            interleave-greedy
            flatten))


(define (any? p xs)
  (if (null? xs)
      #f
      (if (p (car xs))
          #t
          (any? p (cdr xs)))))


(define (any-null? xs) (any? null? xs))


(define (zip . xs)
  (if (or (null? xs) (any-null? xs))
      '()
      (cons
       (map car xs)
       (apply zip (map cdr xs)))))


(define (range from to)
  (define (f left right)
    (if (> left right)
        '()
        (cons left (f (+ 1 left) right))))
  (if (< from to)
      (f from to)
      (reverse (f to from))))



(define (p-either p)
  (lambda (x y)
    (or (p x) (p y))))

(define either-null? (p-either null?))

;; Use this instead?
(define (either p x y) (or (p x) (p y)))


;; A "flat zip",
;; return zs with length (* 2 (min (length xs) (length ys)))
;; and elements from xs ys (x1 y1 x2 y2 ... xn yn)
(define (interleave xs ys)
  (if (either-null? xs ys)
      ;; maybe if xs null, ys & if ys null, xs
      '()
      (cons
       (car xs)
       (cons
        (car ys)
        (interleave (cdr xs) (cdr ys))))))

;; Greedy version of interleave,
;; '(a b c) '(x y) => '(a x b y c)
(define (interleave-greedy xs ys)
  (cond ((null? xs) ys)
        ((null? ys) xs)
        (else (let ((x (car xs))
                  (y (car ys)))
              (cons
               x
               (cons y
                     (interleave-greedy
                      (cdr xs)
                      (cdr ys))))))))

(define (flatten xs)
  (cond ((null? xs) '())
        ((list? (car xs))
         (if (null? (car xs))
             (flatten (cdr xs))
             (cons (car (car xs))
                   (flatten (cons (cdr (car xs)) (cdr xs))))))
        (else (cons (car xs) (flatten (cdr xs))))))

(define (naturals-to k)
  (range 0 k))
