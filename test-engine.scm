(use-modules (srfi srfi-64)
             (proba commands)
             (engine))

(test-group
 "full-diamond"
 (test-equal
  '("a")
   (full-diamond #\a))
 (test-equal
  '(" a "
    "b b"
    " a ")
  (full-diamond #\b))
 (test-equal
  '(" α "
    "β β"
    " α ")
  (full-diamond #\β))
  (test-equal
  '(" Α "
    "Β Β"
    " Α ")
  (full-diamond #\Β)))
